"""
File: boggle.py
Name:
----------------------------------------
TODO:
"""

import time

# This is the file name of the dictionary txt file
# we will be checking if a word exists by searching through it
FILE = 'dictionary.txt'
lst = []


def main():
    """
    TODO:
    """
    start = time.time()
    ####################
    read_dictionary()
    rows = enter()
    ans_lst = []
    has_prefix(rows, ans_lst)

    ####################
    end = time.time()
    print('----------------------------------')
    print(f'The speed of your boggle algorithm: {end - start} seconds.')


def read_dictionary():
    """
    This function reads file "dictionary.txt" stored in FILE
    and appends words in each line into a Python list
    """
    with open(FILE, 'r') as f:
        for line in f:
            t = line.split()
            lst.append(t[0])


def has_prefix(rows, ans_lst):
    """
    :param ans_lst:
    :param rows: (str) A substring that is constructed by neighboring letters on a 4x4 square grid
    :return: (bool) If there is any words with prefix stored in sub_s
    """
    for x in range(0, 4):
        for y in range(0, 4):
            using_index = [(x, y)]
            has_prefix_helper(rows, ans_lst, x, y, using_index, '')




def has_prefix_helper(rows, ans_lst, x, y, using_index, cur_str):
    if cur_str in lst and len(cur_str) >= 4:
        if cur_str not in ans_lst:
            ans_lst.append(cur_str)
            print("found: "+cur_str)
    else:
        for i in range(-1, 2):
            for j in range(-1, 2):
                if 0 <= i+x <= 3 and 0 <= y+j <= 3:
                    if (x+i, y+j) not in using_index:
                        cur_str += rows[x+i][y+j]
                        using_index.append((x+i, y+j))
                        # choose
                        if dict_checker(cur_str):
                            using_index.append((x+i, y+j))
                            # explore
                            has_prefix_helper(rows, ans_lst, x+i, y+j, using_index, cur_str)
                        # un choose
                        cur_str = cur_str[:-1]
                        using_index.pop()




def dict_checker(word):
    count = 0
    checker = True
    while True:
        if count + 1 == len(lst):
            checker = False
            break
        else:
            if not lst[count].startswith(word):
                count += 1
            else:
                break
    return checker


def enter():
    """
    checking if the input is in the right format
    """
    box = []
    while True:
        first = input("1 road in the letters: ")
        if len(first) != 7:
            print('illegal input')
        elif first[1] != ' ' or first[3] != ' ' or first[5] != ' ':
            print('illegal input')
        else:
            second = input("2 road in the letters: ")
            if len(second) != 7:
                print('illegal input')
            elif second[1] != ' ' or second[3] != ' ' or second[5] != ' ':
                print('illegal input')
            else:
                third = input("3 road in the letters: ")
                if len(third) != 7:
                    print('illegal input')
                elif third[1] != ' ' or third[3] != ' ' or third[5] != ' ':
                    print('illegal input')
                else:
                    forth = input("4 road in the letters: ")
                    if len(forth) != 7:
                        print('illegal input')
                    elif forth[1] != ' ' or forth[3] != ' ' or forth[5] != ' ':
                        print('illegal input')
                    else:
                        break
    box.append(first.split())
    box.append(second.split())
    box.append(third.split())
    box.append(forth.split())
    return box







if __name__ == '__main__':
    main()
