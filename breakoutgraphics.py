"""
stanCode Breakout Project
Adapted from Eric Roberts's Breakout by
Sonja Johnson-Yu, Kylie Jue, Nick Bowman, 
and Jerry Liao

YOUR DESCRIPTION HERE
"""
from campy.graphics.gwindow import GWindow
from campy.graphics.gobjects import GOval, GRect, GLabel
from campy.gui.events.mouse import onmouseclicked, onmousemoved
from campy.gui.events.timer import pause
import random
#  constance
BRICK_SPACING = 5      # Space between bricks (in pixels). This space is used for horizontal and vertical spacing.
BRICK_WIDTH = 40       # Height of a brick (in pixels).
BRICK_HEIGHT = 15      # Height of a brick (in pixels).
BRICK_ROWS = 10        # Number of rows of bricks.
BRICK_COLS = 10        # Number of columns of bricks.
BRICK_OFFSET = 50      # Vertical offset of the topmost brick from the window top (in pixels).
BALL_RADIUS = 10       # Radius of the ball (in pixels).
PADDLE_WIDTH = 75      # Width of the paddle (in pixels).
PADDLE_HEIGHT = 15     # Height of the paddle (in pixels).
PADDLE_OFFSET = 50     # Vertical offset of the paddle from the window bottom (in pixels).
#  global variables
INITIAL_Y_SPEED = 7    # Initial vertical speed for the ball.
MAX_X_SPEED = 5        # Maximum initial horizontal speed for the ball.
open = False           # opener


class BreakoutGraphics:

    def __init__(self, ball_radius = BALL_RADIUS, paddle_width = PADDLE_WIDTH,
                 paddle_height = PADDLE_HEIGHT, paddle_offset = PADDLE_OFFSET,
                 brick_rows = BRICK_ROWS, brick_cols = BRICK_COLS,
                 brick_width = BRICK_WIDTH, brick_height = BRICK_HEIGHT,
                 brick_offset = BRICK_OFFSET, brick_spacing = BRICK_SPACING,
                 title='Breakout'):

        # Create a graphical window, with some extra space
        window_width = brick_cols * (brick_width + brick_spacing) - brick_spacing
        window_height = brick_offset + 3 * (brick_rows * (brick_height + brick_spacing) - brick_spacing)
        self.window = GWindow(width=window_width, height=window_height, title=title)

        # Create a paddle
        self.pad_width = paddle_width
        self.pad = GRect(width=paddle_width, height=paddle_height, x=(self.window.width-self.pad_width) / 2
                         , y=self.window.height-paddle_height-paddle_offset)
        self.pad.filled = True
        self.window.add(self.pad)
        self.pad_offset = paddle_offset


        # Center a filled ball in the graphical window
        self.ball_rad = ball_radius
        self.ball = GOval(self.ball_rad * 2, self.ball_rad * 2, x=((self.window.width-self.pad_width) / 2) +
                                                              self.pad_width / 2 - self.ball_rad,
                          y=self.window.height - PADDLE_HEIGHT - paddle_offset - self.ball_rad * 2)
        self.ball.filled = True
        self.window.add(self.ball)


        # Draw bricks
        self.brk_w = brick_width
        self.brk_h = brick_height
        self.row = brick_rows
        self.col = brick_cols
        self.spa = brick_spacing
        self.make_brick()

        # Default initial velocity for the ball
        self._dx = random.randint(0, MAX_X_SPEED)
        self._dy = -INITIAL_Y_SPEED

        # Initialize our mouse listeners
        onmousemoved(self.paddle_position)
        onmouseclicked(self.turn_on)


    def spot_t(self):
        spot_top = self.window.get_object_at(self.ball.x + self.ball_rad, self.ball.y - 1)
        return spot_top


    def spot_r(self):
        spot_right = self.window.get_object_at(self.ball.x + 2 * self.ball_rad + 1,
                                           self.ball.y + self.ball_rad)
        return spot_right


    def spot_l(self):
        spot_left = self.window.get_object_at(self.ball.x - 1, self.ball.y + self.ball_rad)
        return spot_left


    def spot_d(self):
        spot_down = self.window.get_object_at(self.ball.x + self.ball_rad,
                                              self.ball.y + (self.ball_rad * 2) + 1)
        return spot_down


    def new_dx(self, new_X_speed):
        self._dx = new_X_speed


    def new_dy(self, new_y_speed):
        self._dy = new_y_speed



    def paddle_position(self, mouse):
        """
        making the ball move right and left but not over the window
        """
        moving = mouse.x
        if moving < 0:
            moving = 0
        elif moving > self.window.width - self.pad_width:
            moving = self.window.width - self.pad_width
        else:
            moving = mouse.x
        self.window.add(self.pad, x=moving, y=self.window.height - PADDLE_HEIGHT - PADDLE_OFFSET)
        if open == False:
            self.window.add(self.ball, x=moving + self.pad_width / 2 - self.ball_rad / 2,
                            y=self.window.height-self.pad_offset-self.ball_rad*2-self.brk_h)

        if self.ball.y <= self.window.height:
            self.window.add(self.ball)

    @staticmethod
    def close():
        """
        to turn the check off
        """
        global open
        open = False


    @staticmethod
    def on_of_checker():
        """
        making a sensor to make sure the opener is open or not
        """
        return open

    @staticmethod
    def turn_on(event):
        """
        to open the game
        """
        global open
        open = True



    def velocity(self):
        """
        when the ball touch the paddle, change x position
        """
        self._dx = random.randint(0, MAX_X_SPEED)
        if random.random() > 0.5:
            self._dx = -self._dx


    def make_brick(self):
        """
        making the bricks
        """
        y = 0
        for i in range(self.row):
            y = self.spa + self.brk_h + y
            x = 0
            for j in range(self.col):
                reck = GRect(width=self.brk_w, height=self.brk_h, x=x, y=y)
                reck.filled = True
                if y == (self.spa + self.brk_h):
                    reck.fill_color = '#003E3E'
                    reck.color = '#003E3E'
                elif y == 2 * (self.spa + self.brk_h):
                    reck.fill_color = '#005757'
                    reck.color = '#005757'
                elif y == 3 * (self.spa + self.brk_h):
                    reck.fill_color = '#007979'
                    reck.color = '#007979'
                elif y == 4 * (self.spa + self.brk_h):
                    reck.fill_color = '#009393'
                    reck.color = '#009393'
                elif y == 5 * (self.spa + self.brk_h):
                    reck.fill_color = '#00AEAE'
                    reck.color = '#00AEAE'
                elif y == 6 * (self.spa + self.brk_h):
                    reck.fill_color = '#00CACA'
                    reck.color = '#00CACA'
                elif y == 7 * (self.spa + self.brk_h):
                    reck.fill_color = '#00E3E3'
                    reck.color = '#00E3E3'
                elif y == 8 * (self.spa + self.brk_h):
                    reck.fill_color = '#00FFFF'
                    reck.color = '#00FFFF'
                elif y == 9 * (self.spa + self.brk_h):
                    reck.fill_color = '#4DFFFF'
                    reck.color = '#4DFFFF'
                else:
                    reck.fill_color = '#80FFFF'
                    reck.color = '#80FFFF'
                x = self.row + self.brk_w + x
                self.window.add(reck)


    def remove(self, project):
        """
        remove the object that the ball game
        """
        self.window.remove(project)


    @property
    def dx(self):
        return self._dx


    @property
    def dy(self):
        return self._dy


