"""
File: 
Name:Jerry
-------------------------
TODO:
"""

from campy.graphics.gobjects import GOval
from campy.graphics.gwindow import GWindow
from campy.gui.events.timer import pause
from campy.gui.events.mouse import onmouseclicked

VX = 3
DELAY = 10
GRAVITY = 1
SIZE = 20
REDUCE = 0.9
START_X = 30
START_Y = 40
oral = GOval(SIZE, SIZE, x=START_X, y=START_Y)
window = GWindow(800, 500, title='bouncing_ball.py')
time = 0  # after 3times will stop
stop = False
def main():
    """
    This program simulates a bouncing ball at (START_X, START_Y)
    that has VX as x velocity and 0 as y velocity. Each bounce reduces
    y velocity to REDUCE of itself.
    """
    global stop
    oral.filled = True
    oral.fill_color = 'black'
    while time < 3:
        window.add(oral, START_X, START_Y)
        pause(DELAY)
        if not stop:
            onmouseclicked(stop_it)
        else:
            drop(oral)
    window.add(oral, START_X, START_Y)


def drop(oral):
    """
    making the ball rebounce, consider the ground,
    and the top of the tall that the ball can re-
    bounce.
    """
    global time, stop
    x = 0  # to prevent the ball haven't bounce to ground then rebounce
    k = 0  # original speed
    while True:
        oral.move(VX, k)
        k += GRAVITY
        if oral.y - SIZE >= window.height and x % 2 == 0:  # fall to ground and rebounce
            k = -k
            k *= REDUCE
            x += 1
        elif k >= 0 and x % 2 != 0:  # k > 0 is the acceleration from negative to positive
            k = 0  # when the ball goes to the highest spot, the velocity is zero
            x += 1
        pause(DELAY)
        if oral.x - SIZE >= window.width:
            time += 1
            stop = False
            break




def stop_it(stay):
    global stop
    stop = True


if __name__ == "__main__":
    main()
