"""
stanCode Breakout Project
Adapted from Eric Roberts's Breakout by
Sonja Johnson-Yu, Kylie Jue, Nick Bowman,
and Jerry Liao.

YOUR DESCRIPTION HERE
"""

from campy.gui.events.timer import pause
from breakoutgraphics import BreakoutGraphics

FRAME_RATE = 1000 / 120  # 120 frames per second
NUM_LIVES = 3  # Number of attempts
live = 0


def main():
    global live
    graphics = BreakoutGraphics()

    while live < 3:
        pause(FRAME_RATE)
        if graphics.on_of_checker():
            graphics.ball.move(graphics.dx, graphics.dy)
            t = graphics.spot_t()  # sensor's top
            l = graphics.spot_l()  # sensor's left
            r = graphics.spot_r()  # sensor's right
            d = graphics.spot_d()  # sensor's down
            if t and l and r and d is None:
                graphics.ball.move(graphics.dx, graphics.dy)
            elif t is not None:  # if top touch the brick
                graphics.new_dy(-graphics.dy)
                graphics.remove(t)
            elif graphics.ball.x <= 0 or graphics.ball.x >= graphics.window.width-graphics.ball_rad*2:  # if ball touch the wall
                graphics.new_dx(-graphics.dx)
            elif l is not None:  # if left side touch the brick
                graphics.remove(l)
                graphics.new_dx(-graphics.dx)
            elif r is not None:  # if right touch the brick
                graphics.remove(r)
                graphics.new_dx(-graphics.dx)
            elif d is not None:  # if bottom touch the paddle
                graphics.velocity()
                graphics.new_dy(-graphics.dy)
            elif graphics.ball.y <= 0:
                graphics.new_dy(-graphics.dy)

            elif graphics.ball.y >= graphics.window.height:  # if ball touch ground
                graphics.close()
                graphics.new_dy(-graphics.dy)
                # graphics.make_brick()
                live += 1






if __name__ == '__main__':
    main()
