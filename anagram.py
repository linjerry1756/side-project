"""
File: anagram.py
Name:
----------------------------------
This program recursively finds all the anagram(s)
for the word input by user and terminates when the
input string matches the EXIT constant defined
at line 19

If you correctly implement this program, you should see the
number of anagrams for each word listed below:
    * arm -> 3 anagrams
    * contains -> 5 anagrams
    * stop -> 6 anagrams
    * tesla -> 10 anagrams
    * spear -> 12 anagrams
"""

import time                   # This file allows you to calculate the speed of your algorithm

# Constants
FILE = 'dictionary.txt'       # This is the filename of an English dictionary
EXIT = '-1'                   # Controls when to stop the loop
lst = []


def main():
    """
    TODO:
    """
    start = time.time()
    ####################
    read_dictionary()
    while True:
        separate = []
        word = input('welcome to stanCode \"Anagram Generator\" (or -1 to quit) ')
        print('searching...')
        if word == EXIT:
            break
        else:
            for i in range(len(word)):
                separate.append(word[i])
            words = find_anagrams(separate)
            print(len(words), 'anagrams', words)
    ####################
    end = time.time()
    print('----------------------------------')
    print(f'The speed of your anagram algorithm: {end-start} seconds.')


def read_dictionary():
    with open(FILE, 'r') as f:
        for line in f:
            t = line.split()
            lst.append(t[0])




def find_anagrams(s):
    """
    :param s:
    :return:
    """
    trans = []               # let every word be a independent object  even it is the same letter
    for i in range(len(s)):  # to transfer as a number is to catch the word position easily
        trans.append(i)

    return find_anagrams_help(s, [], len(s), trans, [], [], [], [])


def find_anagrams_help(s, found, count, transfer, in_dict, not_in_dict, transfer_box, total):
    if count == len(found):
        word = ''
        for i in range(len(found)):
            word += found[i]
        if word in total:
            pass
        else:
            if word in lst:
                print('found  :', word)
                print('Searching...')
                total.append(word)

    else:
        for num in transfer:
            if num in transfer_box:
                pass
            else:
                string = ''
                found = []
                transfer_box.append(num)
                for j in range(len(transfer_box)):
                    found.append(s[transfer_box[j]])  # transfer the number to word
                    string += found[j]
                if string in in_dict:
                    in_dict.append(has_prefix(string))
                    find_anagrams_help(s, found, count, transfer, in_dict, not_in_dict, transfer_box, total)
                    transfer_box.pop()
                elif string in not_in_dict:
                    transfer_box.pop()
                else:
                    if has_prefix(string):
                        in_dict.append(string)
                        find_anagrams_help(s, found, count, transfer, in_dict, not_in_dict, transfer_box, total)
                        transfer_box.pop()
                    else:
                        not_in_dict.append(string)
                        transfer_box.pop()
    return total






def has_prefix(sub_s):
    """
    :param sub_s:
    :return:
    """
    count = 0
    checker = True
    while True:
        if count + 1 == len(lst):
            checker = False
            break
        else:
            if not lst[count].startswith(sub_s):
                count += 1
            else:
                break
    return checker


if __name__ == '__main__':
    main()
