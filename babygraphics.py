"""
SC101 Baby Names Project
Adapted from Nick Parlante's Baby Names assignment by
Jerry Liao.

YOUR DESCRIPTION HERE
"""

import tkinter
import babynames
import babygraphicsgui as gui

FILENAMES = [
    'data/full/baby-1900.txt', 'data/full/baby-1910.txt',
    'data/full/baby-1920.txt', 'data/full/baby-1930.txt',
    'data/full/baby-1940.txt', 'data/full/baby-1950.txt',
    'data/full/baby-1960.txt', 'data/full/baby-1970.txt',
    'data/full/baby-1980.txt', 'data/full/baby-1990.txt',
    'data/full/baby-2000.txt', 'data/full/baby-2010.txt'
]
CANVAS_WIDTH = 1000
CANVAS_HEIGHT = 600
YEARS = [1900, 1910, 1920, 1930, 1940, 1950, 1960, 1970, 1980, 1990, 2000, 2010]
GRAPH_MARGIN_SIZE = 20
COLORS = ['red', 'purple', 'green', 'blue']
TEXT_DX = 2
LINE_WIDTH = 2
MAX_RANK = 1000


def get_x_coordinate(width, year_index):
    """
    Given the width of the canvas and the index of the current year
    in the YEARS list, returns the x coordinate of the vertical
    line associated with that year.

    Input:
        width (int): The width of the canvas
        year_index (int): The index of the current year in the YEARS list
    Returns:
        x_coordinate (int): The x coordinate of the vertical line associated
                              with the specified year.
    """
    line = (width - 2 * GRAPH_MARGIN_SIZE) // len(YEARS)
    x = GRAPH_MARGIN_SIZE + year_index * line
    return x




def draw_fixed_lines(canvas):
    """
    Erases all existing information on the given canvas and then
    draws the fixed background lines on it.

    Input:
        canvas (Tkinter Canvas): The canvas on which we are drawing.

    Returns:
        This function does not return any value.
    """
    canvas.delete('all')            # delete all existing lines from the canvas

    # Write your code below this line
    #################################

    canvas.create_line(0, CANVAS_HEIGHT - GRAPH_MARGIN_SIZE, CANVAS_WIDTH, CANVAS_HEIGHT - GRAPH_MARGIN_SIZE,
                       width=LINE_WIDTH, fill='black')  # the row lines
    canvas.create_line(0, GRAPH_MARGIN_SIZE, CANVAS_WIDTH, GRAPH_MARGIN_SIZE, width=LINE_WIDTH, fill='black')

    for i in range(len(YEARS)):
        canvas.create_line(get_x_coordinate(CANVAS_WIDTH, i), 0, get_x_coordinate(CANVAS_WIDTH, i), CANVAS_HEIGHT,
                           width=LINE_WIDTH, fill='black')  # the column lines
        # canvas.create_text(get_x_coordinate(CANVAS_WIDTH, i)+LINE_WIDTH, CANVAS_HEIGHT - GRAPH_MARGIN_SIZE + LINE_WIDTH,
        #                    text=YEARS[i], anchor=tkinter.NW, width=LINE_WIDTH, font='times 10')
        canvas.create_text(get_x_coordinate(CANVAS_WIDTH, i)+LINE_WIDTH, CANVAS_HEIGHT - GRAPH_MARGIN_SIZE + LINE_WIDTH,
                           text=YEARS[i], anchor=tkinter.NW, font='times 10')




def draw_names(canvas, name_data, lookup_names):
    """
    Given a dict of baby name data and a list of name, plots
    the historical trend of those names onto the canvas.

    Input:
        canvas (Tkinter Canvas): The canvas on which we are drawing.
        name_data (dict): Dictionary holding baby name data
        lookup_names (List[str]): A list of names whose data you want to plot

    Returns:
        This function does not return any value.
    """
    draw_fixed_lines(canvas)        # draw the fixed background grid

    # Write your code below this line
    #################################
    color = 0
    size = (CANVAS_HEIGHT - 2 * GRAPH_MARGIN_SIZE) / 1000
    for i in range(len(lookup_names)):
        if color >= len(COLORS):
            color = 0
        else:
            line_color = COLORS[color]
            color += 1

        name = lookup_names[i]
        old_box = []
        r = 0  # to find out which value have to sand back to get_x_coordinate
        data_size = 0
        for year, rank in name_data[name].items():
            data_size += 1
            while True:
                """
                this loop is to check if the name in each is out of rank 1000
                """
                print(int(year))
                print(r)
                if YEARS[r] == int(year):  # if the data's year is same with the year in the window
                    break
                else:
                    if len(old_box) == 0:  # if the data's year is different with the year in the window and it was the first data
                        old_box.append('nothing')
                        old_box.append(1000)
                        #  print first text
                        canvas.create_text(get_x_coordinate(CANVAS_WIDTH, r) + LINE_WIDTH,
                                           CANVAS_HEIGHT - GRAPH_MARGIN_SIZE,
                                           text=name + '*', anchor=tkinter.NW, font='times 10', fill=line_color)

                    else:  # if it was not the first data
                        canvas.create_line(get_x_coordinate(CANVAS_WIDTH, r-1), int(old_box[1]) * size + GRAPH_MARGIN_SIZE,
                                           get_x_coordinate(CANVAS_WIDTH, r),
                                           CANVAS_HEIGHT - GRAPH_MARGIN_SIZE, width=LINE_WIDTH, fill=line_color)
                        canvas.create_text(get_x_coordinate(CANVAS_WIDTH, r) + LINE_WIDTH,
                                           CANVAS_HEIGHT - GRAPH_MARGIN_SIZE,
                                           text=name+'*', anchor=tkinter.NW, font='times 10', fill=line_color)
                        old_box = [year, 1000]
                    r += 1
                    data_size += 1

            if len(old_box) == 0:  # if the data's year is same with the year in the window
                old_box.append(year)
                old_box.append(rank)
                canvas.create_text(get_x_coordinate(CANVAS_WIDTH, r) + LINE_WIDTH,
                                   int(rank) * size + GRAPH_MARGIN_SIZE,
                                   text=name+rank, anchor=tkinter.NW, font='times 10', fill=line_color)
                r += 1
            else:

                canvas.create_line(get_x_coordinate(CANVAS_WIDTH, r-1), int(old_box[1]) * size + GRAPH_MARGIN_SIZE,
                                   get_x_coordinate(CANVAS_WIDTH, r),
                                   int(rank) * size + GRAPH_MARGIN_SIZE, width=LINE_WIDTH, fill=line_color)
                canvas.create_text(get_x_coordinate(CANVAS_WIDTH, r) + LINE_WIDTH,
                                   int(rank) * size + GRAPH_MARGIN_SIZE,
                                   text=name+rank, anchor=tkinter.NW, font='times 10', fill=line_color)

                r += 1
                old_box = [year, rank]
        if data_size < len(YEARS):
            """
            if the name in last few decade. the rank is out of 1000 then draw the lines in the bottom
            """
            for k in range(len(YEARS)-data_size):
                canvas.create_line(get_x_coordinate(CANVAS_WIDTH, r - 1), int(old_box[1]) * size + GRAPH_MARGIN_SIZE,
                                   get_x_coordinate(CANVAS_WIDTH, r),
                                   CANVAS_HEIGHT - GRAPH_MARGIN_SIZE, width=LINE_WIDTH, fill=line_color)
                canvas.create_text(get_x_coordinate(CANVAS_WIDTH, r) + LINE_WIDTH,
                                   CANVAS_HEIGHT - GRAPH_MARGIN_SIZE,
                                   text=name + '*', anchor=tkinter.NW, font='times 10', fill=line_color)
                old_box = ['nothing', 1000]
                r += 1


# main() code is provided, feel free to read through it but DO NOT MODIFY

def main():
    # Load data
    name_data = babynames.read_files(FILENAMES)

    # Create the window and the canvas
    top = tkinter.Tk()
    top.wm_title('Baby Names')
    canvas = gui.make_gui(top, CANVAS_WIDTH, CANVAS_HEIGHT, name_data, draw_names, babynames.search_names)

    # Call draw_fixed_lines() once at startup so we have the lines
    # even before the user types anything.
    draw_fixed_lines(canvas)

    # This line starts the graphical loop that is responsible for
    # processing user interactions and plotting data
    top.mainloop()


if __name__ == '__main__':
    main()
